export declare function each(promises: (Promise<any> | any)[], cb: (value: any, index?: number, length?: number) => Promise<any>): Promise<any[]>;
