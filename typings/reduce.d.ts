declare type cb = (value: any, index?: number, length?: number) => Promise<any>;
export declare function reduce(iterable: (Promise<any> | any)[], reducer: cb, initial: any): Promise<any>;
export {};
