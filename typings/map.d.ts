export declare function mapParallel(promises: (Promise<any> | any)[], cb: (value: any, index?: number, length?: number) => Promise<any>): Promise<any[]>;
export declare function mapSeries(promises: (Promise<any> | any)[], cb: (value: any, index?: number, length?: number) => Promise<any>): Promise<any[]>;
