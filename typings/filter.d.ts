export declare function filterParallel(promises: (Promise<any> | any)[], cb: (value: any, index?: number, length?: number) => Promise<any>): Promise<any[]>;
export declare function filterSeries(promises: (Promise<any> | any)[], cb: (value: any, index?: number, length?: number) => Promise<any>): Promise<any[]>;
