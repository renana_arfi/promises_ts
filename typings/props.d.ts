interface inputProps {
    [key: string]: Promise<any> | any;
}
export declare function props(input: inputProps): Promise<any>;
export {};
