export async function all(promises: Promise<any>[]): Promise<any[]> {
    const results: any[] = [];
    for (const p of promises) {
        results.push(await p);
    }
    return results;
}
