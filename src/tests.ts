import { all, delay, each, filterParallel, filterSeries, mapParallel, mapSeries, props, race, reduce ,some} from "./index.js";


// test for "all" fun
const promise1 = Promise.resolve(42);
const promise2 = Promise.resolve(true);
const promise3 = new Promise((resolve) => {
    setTimeout(resolve, 3000, "foo");
});

all([promise1, promise2, promise3]).then((values) => {
    console.log(values);
});

// test for "delay" fun
const str = "geronimo";
async function print(input: any, ms: number) {
    for (const char of input) {
        await delay(ms);
        console.log(char);
    }
}
print(str, 1000);

// test for "each" fun

const items = [Promise.resolve(0), Promise.resolve(1), Promise.resolve(2)];

each(items, item => {
    console.log("start processing item:", item);
    const random_delay = Math.random() * 500;

    return delay(random_delay)
        .then(() => {
            console.log("async Operation Finished. item:", item);
        });
})

    .then(originalArray => {
        console.log("All tasks are done now...", originalArray);
    })

    .catch((error) => console.log(error));

// test for "map" fun
const items1 = ["0", 1, 2, 3, 4, 5, 6, 7, 8, 9];
mapSeries(items1, item => {

    console.log("start processing item:", item);
    return delay(Math.random() * 2000)
        .then(() => {
            console.log("async Operation Finished. item:", item);
            return item * 2;
        });
})
    .then(resultArray => {
        console.log("All tasks are done now...", resultArray);
    })
    .catch(error => console.log(error));

mapParallel(items, item => {

    console.log("start processing item:" ,item);
    return delay(Math.random() * 2000)
                  .then(()=> {
                      console.log("async Operation Finished. item:", item); 
                      return item * 2;
                  });
})
.then( resultArray => {
    console.log("All tasks are done now...",resultArray);
})
.catch(error => console.log(error));

// test for "filter" fun

const isOdd = async function (x: any) {
    return (x % 2 === 0);
};
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

filterParallel(arr, isOdd)
    .then(console.log);

filterSeries(arr, isOdd)
    .then(console.log);

// test for "props" fun

const obj = { promise1, promise2,promise3};
props(obj).then((values) => {
  console.log(values);
});

// test for "race" fun
const promise5 = new Promise((resolve) => {
    setTimeout(resolve, 500, "one");
});

const promise4 = new Promise((resolve) => {
    setTimeout(resolve, 100, "two");
});

race([promise5, promise4]).then((value) => {
    console.log(value);
    // Both resolve, but promise2 is faster
});

// test for "reduce" fun

reduce([1, 3, 5, 7, 9], async (seq, n) => {
    await delay(1000);
    return seq + n;
}, 5).then(
    (arr) => console.log("done", arr),
    (e) => console.log(e)
);

// test for "some" fun

const promise6 = new Promise((resolve) => {
    setTimeout(resolve, 500, "one");
});

const promise7 = new Promise((resolve) => {
    setTimeout(resolve, 100, "two");
});
const promise8 = new Promise((resolve) => {
    setTimeout(resolve, 300, "three");
});

some([promise6, promise7,promise8], 2).then((value) => {
    console.log(value);
    // Both resolve, but promise2 is faster
});








