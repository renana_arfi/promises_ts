export const race = (iterable:Promise<any>[]) =>
    new Promise((res, rej) => {
        iterable.forEach(item => item.then(res).catch(rej));
    });

