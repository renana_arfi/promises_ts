export async function each(promises: (Promise<any> | any)[], cb: (value: any,index?:number,length?:number) => Promise<any>): Promise<any[]> {
    const items = [];
    for (let item of promises) {
        item = await item;
        await cb(item);
        items.push(item);
    }
    return items;
}
