export async function filterParallel(promises: (Promise<any> | any)[], cb: (value: any, index?: number, length?: number) => Promise<any>): Promise<any[]> {

    const arrPending = promises.map(async (iter, i, iterable) => {
        iter = await iter;
        return cb(iter, i, iterable.length);
    });

    const results = [];
    for (let i = 0; i < arrPending.length; i++) {
        const result = await arrPending[i];
        if (result) {
            results.push(await promises[i]);
        }
    }
    return results;
}

export async function filterSeries(promises: (Promise<any> | any)[], cb: (value: any, index?: number, length?: number) => Promise<any>): Promise<any[]> {
    const results = [];
    for (let item of promises) {
        item = await item;
        const result = await cb(item);

        if (result) {
            await results.push(item);
        }
    }
    return results;
}

