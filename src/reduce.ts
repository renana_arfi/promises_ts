type cb =  (value: any, index?: number, length?: number) => Promise<any>;

export async function reduce(iterable:(Promise<any>|any)[], reducer:cb, initial:any) {
    let value = await initial || await iterable.shift();
    for (let item of iterable) {
        item = await item;
        value = await reducer(value, item);
    }
    return value;
}
