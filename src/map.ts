
export async function mapParallel(promises: (Promise<any> | any)[], cb: (value: any, index?: number, length?: number) => Promise<any>): Promise<any[]> {

    const arrPending = promises.map((iter, i) => cb(iter, i, promises.length));

    const results = [];
    for (const item of arrPending) {
        results.push(await item);
    }
    return results;
}

export async function mapSeries(promises: (Promise<any> | any)[], cb: (value: any, index?: number, length?: number) => Promise<any>): Promise<any[]> {
    const results = [];
    for (let item of promises) {
        item = await item;
        const result = await cb(item);
        results.push(result);
    }
    return results;
}