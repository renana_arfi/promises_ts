
export function some (iterable:Promise<any>[], count:number) {
    return new Promise(async res => {
        const arr :any[]= [];
        iterable = await iterable;
        iterable.forEach(async item => {
            item.then(item => {
                if (arr.length < count) {
                    arr.push(item);
                }
                else res(arr);
            });
        });
    });
}