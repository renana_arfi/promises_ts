interface inputProps{
     [key:string]:Promise<any> |any;
}
export async function props(input:inputProps):Promise<any>{

    const results:inputProps = {};
    for (const key in input) {
        results[key] = await input[key];
    }
    return results;
}
